# Script Name: tea_time_class_2.R
# Purpose: Learning R
# Authors: Jack Mousseau



# install a collection of datasets
if (!require("datasets")) install.packages("datasets");
library(datasets)

# View the "mtcars" data frame
View(mtcars)


########################################################
## Create a line graph                                ##
########################################################

# Expose the columns of "mtcars" data set. Without this
# line of code one would have to access columns by
# mtcars$<name> where <name> is the column name. For
# instance: mtcars$mpg
attach(mtcars)

# Plot weight vs. mpg

# Create a linear model

# Add a straight line to the plot

# Set the plot title


########################################################
## Create a histogram                                 ##
########################################################

# Simple histogram

# Colored Histogram with Different Number of Bins


########################################################
## Create a dot plot                                  ##
########################################################

# Simple Dotplot

# Dotplot: Grouped Sorted and Colored
# Sort by mpg, group and color by cylinder

# sort by mpg

# cylinder must be a factor

# Set the color based on the number of cylinders

########################################################
## Create a dot plot                                  ##
########################################################

# Simple Pie Chart
slices = c(10, 12, 4, 16, 8)

# labels for the respective slices
lbls = c("US", "UK", "Australia", "Germany", "France")

# generate the pie chart


# Pie Chart with Percentages
# compute the percents

# add percents to labels

# ad % to labels

# generate the pie chart



#########################################################
# GGplot2
#########################################################
install.packages("ggplot2")
library(ggplot2)


# CITE: http://blog.echen.me/2012/01/17/quick-introduction-to-ggplot2/

# Grab some example data to work with, as IRIS comes with ggplot2

head(iris) # by default, head displays the first 6 rows. see `?head`
head(iris, n = 10) # we can also explicitly set the number of rows to display

qplot(Sepal.Length, Petal.Length, data = iris)
# Plot Sepal.Length vs. Petal.Length, using data from the `iris` data frame.
# * First argument `Sepal.Length` goes on the x-axis.
# * Second argument `Petal.Length` goes on the y-axis.
# * `data = iris` means to look for this data in the `iris` data frame. 


qplot(Sepal.Length, Petal.Length, data = iris, color = Species)

qplot(Sepal.Length, Petal.Length, data = iris, color = Species, size = Petal.Width)
# We see that Iris setosa flowers have the narrowest petals.


qplot(Sepal.Length, Petal.Length, data = iris, color = Species, size = Petal.Width, alpha = I(0.7))
# By setting the alpha of each point to 0.7, we reduce the effects of overplotting.

qplot(Sepal.Length, Petal.Length, data = iris, color = Species,
      xlab = "Sepal Length", ylab = "Petal Length",
      main = "Sepal vs. Petal Length in Fisher's Iris data")

## Another example with Orange Tree Data

# `Orange` is another built-in data frame that describes the growth of orange trees.
qplot(age, circumference, data = Orange, geom = "line",
      colour = Tree,
      main = "How does orange tree circumference vary with age?")


### More examples and class factor

# CITE: http://www.statmethods.net/advgraphs/ggplot2.html

# create factors with value labels
mtcars$gear <- factor(mtcars$gear,levels=c(3,4,5),
                      labels=c("3gears","4gears","5gears"))
mtcars$am <- factor(mtcars$am,levels=c(0,1),
                    labels=c("Automatic","Manual"))
mtcars$cyl <- factor(mtcars$cyl,levels=c(4,6,8),
                     labels=c("4cyl","6cyl","8cyl"))

# Kernel density plots for mpg
# grouped by number of gears (indicated by color)
qplot(mpg, data=mtcars, geom="density", fill=gear, alpha=I(.5),
      main="Distribution of Gas Milage", xlab="Miles Per Gallon",
      ylab="Density")

# Scatterplot of mpg vs. hp for each combination of gears and cylinders
# in each facet, transmittion type is represented by shape and color
qplot(hp, mpg, data=mtcars, shape=am, color=am,
      facets=gear~cyl, size=I(3),
      xlab="Horsepower", ylab="Miles per Gallon")

# Separate regressions of mpg on weight for each number of cylinders
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"),
      method="lm", formula=y~x, color=cyl,
      main="Regression of MPG on Weight",
      xlab="Weight", ylab="Miles per Gallon")

# Boxplots of mpg by number of gears
# observations (points) are overlayed and jittered
qplot(gear, mpg, data=mtcars, geom=c("boxplot", "jitter"),
      fill=gear, main="Mileage by Gear Number",
      xlab="", ylab="Miles per Gallon") 



### Real life example
# We need to get the data and make sure our working directory

# PRO TIP: Shortcut for changing working is "control+shift+h"

data.temp = read.csv("noaa_weather.csv")

data.temp$Date.Time = as.POSIXct(data.temp$Date.Time)


plot1 = ggplot(data = data.temp, aes(x = Date.Time, y =Temperature, colour = Climate)) +  
  facet_grid(Climate ~ ., scales = "free") + 
  geom_line(size = 0.75) + ggtitle("NOAA Temperature Data For Building Climates") +
  theme(axis.title=element_text(size = 15, face = "bold")) + ylab("Temperature (F)") + xlab("Date") +
  theme(plot.title=element_text(size = 15, face = "bold")) +
  theme(strip.text = element_text(size = 12, face = "bold")) +
  theme(legend.position = "none")
plot1



data.energy = data.temp
data.energy = data.energy[-c(1:96),]
data.energy = data.energy[-c((673):(length(data1[,1])-96)),]
data.energy = data.energy[-c((1345):length(data.energy[,1])),]


plot2 = ggplot(data = data.energy, aes(x = Date.Time, y =Temperature, colour = Climate)) +  
  facet_grid(Climate ~ ., scales = "free") + 
  geom_line(size = 0.75) + ggtitle("NOAA Temperature Data For Building Climates - One Week") +
  theme(axis.title=element_text(size = 15, face = "bold")) + ylab("Temperature (F)") + xlab("Date") +
  theme(plot.title=element_text(size = 15, face = "bold")) +
  theme(strip.text = element_text(size = 12, face = "bold")) +
  theme(legend.position = "none")
plot2

install.packages("Rmisc")
library(Rmisc)

multiplot(plot1, plot2)

