Teatime Topics
-----------------

#####################################################################################
W1:
P1+P2 :Basic R
	  RStudio Landscape
       	  Variables, Syntaxes, Classes
          Scripts, headers & Licenses
	  For Loops and IF statements
	  Basic Functions, which, length, mean, sd, etc.
	  Installing packages, library()
P2+P3: Graphics
	  Review Basic R
	  Basic Graphics, GGplot, pairwise plots - Today's Tea Time Lesson

##########################################################################################
W2:
P1+P2: Graphics
	  Review Basic R
	  Basic Graphics, GGplot, pairwise plots - Today's Tea Time Lesson
P2+P3:Git 
        Version Control System, Workflow
        Command Line Intro


##########################################################################################
W3:
P1 :Linear Regression/Modeling 
	  Run through example dataset making various regression models
	  Assessing fit and R squared, predicting values, etc.
P2: Data Manipulation 
	  NAs, Binding data, Deleting rows, date formats
	  Clustering

P3: Other Data Fitting  
	  Peak fitting and Hyperspec
    Package Creation
	  Run through creating all aspects necessary for basic packages

###########################################################################################
W4:
P1: Basic Python
        Syntax, Numerical variable types
        Attributes
        Loops,if statements, functions
        File Input/Output
P2: Numpy
        Package import in Python
        Indexing
        Array Slicing
P3: Python Plotting Intro
        Basic Plotting
        Plot Options
        Bar Charts, histograms
        Image manipulation
        Subplots
        Plot saving

###########################################################################################
W5:
P1: SciPy
        Interpolation and Derivatives
        Numeric intergrals
        Ordinary differential equations, fitting, linear algebra
        Fast Fourier Transform 
P2: Image Processing with Python
        Image import
        Binarizing, histogram equalization, blurring,filter
P3: Pandas
        Dataset processing basic functions: index, plot, transpose, delete column, merging

############################################################################################
W6:
P1: rPython
        python assign, execute, command call in R
    Intro to Latex
        Software
        Basic commands in Latex: spaces, text style, special characters etc.
P2: Intro to Latex
        Basic commands in Latex: image and table insert, citations, centering
P3: CV writing
        Writing tips
    Rmarkdown
        Knit, plot and table inserting, bullets adding

#############################################################################################
W7:
P1: Fixed and Mixed effects Modeling
        Intro to fixed and mixed effect modeling
        Example of Modeling
P2: Semi-gSEM package in R
        Intro to semi-gSEM package
        Example of semi-gSEM package application


