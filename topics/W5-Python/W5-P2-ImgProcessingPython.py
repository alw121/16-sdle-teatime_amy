# -*- coding: utf-8 -*-
"""
Created on July 05, 2016
Author(s): Justin S. Fada
License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
"""

import numpy
import numpy as np
from numpy import array

import scipy
import scipy as sp
from scipy import ndimage
import scipy.misc

import cv2

from matplotlib import pyplot as plt

import os
from os import listdir
from os.path import isfile, join

import Image, ImageDraw

import skimage
from skimage import morphology
from skimage import io
from skimage import img_as_ubyte
from skimage.morphology import square
from skimage import transform
from skimage.transform import (hough_line, hough_line_peaks, probabilistic_hough_line)
from skimage import filter
from skimage import data


# path to image file
filePath = '/home/parallels/Documents/Python/Data/MiniModuleSet-NicksStudy/sa23058_00-05-05-01-elo.JPG'

# read in RGB image file as grey
grayModuleImgTest = skimage.io.imread(filePath, as_grey=True)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(grayModuleImgTest, cmap=plt.cm.gray, interpolation='none')
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(grayModuleImgTest, cmap=plt.cm.spectral, interpolation='none')

#colorImg = grayModuleImgTest = skimage.io.imread(filePath)
#fig, ax = plt.subplots(figsize=(4, 3))
#ax.imshow(colorImg, cmap=plt.cm.gray, interpolation='none')

# bilateral denoise
ImgBilateral = skimage.filter.denoise_bilateral(grayModuleImgTest)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgBilateral, cmap=plt.cm.gray, interpolation='none')
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgBilateral, cmap=plt.cm.spectral, interpolation='none')

"""
A bilateral filter is a non-linear, edge-preserving and noise-reducing smoothing
 filter for images. The intensity value at each pixel in an image is replaced by
 a weighted average of intensity values from nearby pixels. This weight can be based 
 on a Gaussian distribution. Crucially, the weights depend not only on Euclidean distance 
 of pixels, but also on the radiometric differences (e.g. range differences, such as color 
 intensity, depth distance, etc.). This preserves sharp edges by systematically looping 
 through each pixel and adjusting weights to the adjacent pixels accordingly.
"""

# adaptive threshold
block_size = 500
binary_adaptive = skimage.filter.threshold_adaptive(ImgBilateral, block_size)
binary_adaptive = binary_adaptive.astype(int)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(binary_adaptive, cmap=plt.cm.gray, interpolation='none')

# global threshold
sd = np.std(ImgBilateral)
avg = np.average(ImgBilateral)
ImgBilateral[ImgBilateral <= (avg+(sd*.35))] = 0
ImgBilateral[ImgBilateral >(avg+(sd*.35))] = 1
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgBilateral, cmap=plt.cm.gray, interpolation='none')

# Composite adaptive and global threshold
ImgComposite = numpy.add(ImgBilateral,binary_adaptive)
ImgComposite[ImgComposite <= 1.5]=0
ImgComposite[ImgComposite > 1.5]=1
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgComposite, cmap=plt.cm.gray, interpolation='none')

# closes up the ends of the busbars
ImgClose = ndimage.binary_closing(ImgComposite, structure=np.ones((20,20))).astype(np.int)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgClose, cmap=plt.cm.gray, interpolation='none')

# computes convex hull to surround cells
ImgHull = skimage.morphology.convex_hull_object(ImgClose)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgHull, cmap=plt.cm.gray, interpolation='none')

# identifies the unique objects
labeled_array, num_features = scipy.ndimage.measurements.label(ImgHull)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(labeled_array, cmap=plt.cm.spectral, interpolation='none')



### new cell area type to extract################
grayModuleImgTest2 = img_as_ubyte(grayModuleImgTest)
ImgNLdenoise = cv2.fastNlMeansDenoising(grayModuleImgTest2, h = 10) ### needs to take int8...
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgNLdenoise, cmap=plt.cm.gray, interpolation='none')
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgNLdenoise, cmap=plt.cm.spectral, interpolation='none')

"""
Non-local means is an algorithm in image processing for image denoising. 
Unlike "local mean" filters, which take the mean value of a group of pixels
surrounding a target pixel to smooth the image, non-local means filtering 
takes a mean of all pixels in the image, weighted by how similar these pixels 
are to the target pixel.
"""

# another adpative filter
ImgAdaptiveGaussian = cv2.adaptiveThreshold(ImgNLdenoise,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,75,2)
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgAdaptiveGaussian, cmap=plt.cm.gray, interpolation='none')

# global threshold NL denoise
sd = np.std(ImgNLdenoise)
avg = np.average(ImgNLdenoise)
ImgNLdenoise[ImgNLdenoise <= (avg+(sd*.25))] = 0
ImgNLdenoise[ImgNLdenoise >(avg+(sd*.25))] = 255
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgNLdenoise, cmap=plt.cm.gray, interpolation='none')

# Composite adaptive and global threshold
ImgNLdenoise = ImgNLdenoise/2
ImgAdaptiveGaussian = ImgAdaptiveGaussian/2

ImgCompositeNL = np.add(ImgNLdenoise,ImgAdaptiveGaussian)
ImgCompositeNL[ImgCompositeNL <= .75*255]=0
ImgCompositeNL[ImgCompositeNL > .75*255]=1
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(ImgCompositeNL, cmap=plt.cm.gray, interpolation='none')

#ImgOpen = skimage.morphology.opening(ImgCompositeNL,square(20))
#fig, ax = plt.subplots(figsize=(4, 3))
#ax.imshow(ImgOpen, cmap=plt.cm.gray, interpolation='none')
#savePath = '/home/parallels/Documents/Python/1606-ParsedMiniOutputs'

# pulls out one cells area. In this case, from ImgCompositeNL. Works for any image.

k = 4 ## k = 1 just for example purposes 
locationY, locationX = np.where(labeled_array == k)
Xmin = np.min(locationX)
Xmax = np.max(locationX)
Xdiff = Xmax - Xmin
Ymin = np.min(locationY)
Ymax = np.max(locationY)
Ydiff = Ymax - Ymin
canvas = np.zeros([Ydiff+1,Xdiff+1])
dims = locationX.shape
if (Xdiff >= 400):
    for m in range(dims[0]):
        canvas[(locationY[m]-Ymin),(locationX[m]-Xmin)] = ImgCompositeNL[locationY[m],locationX[m]]
    #reducedCanvas = skimage.transform.resize(canvas, [200,200])
    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(canvas, cmap=plt.cm.gray, interpolation='none')
   
plt.savefig('Img4.eps', format='eps', dpi=1000)

# manipulates values for following functions
extractedImg = img_as_ubyte(reducedCanvas)
avg = np.average(extractedImg)
extractedImg[extractedImg <= avg] = 0
extractedImg[extractedImg > avg] = 255

# uses canny to get edges (you could use sobel as well)
#sobelHorz = skimage.filter.sobel(extractedImg)
edges = skimage.filter.canny(extractedImg)

# hough line transform to get edges of busbars. Run to plt.show()
lines = probabilistic_hough_line(edges, threshold=12, line_length=100, line_gap=9)

fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(8,4), sharex=True, sharey=True)

ax1.imshow(extractedImg, cmap=plt.cm.gray)
ax1.set_title('Input image')
ax1.set_axis_off()
ax1.set_adjustable('box-forced')

ax2.imshow(edges, cmap=plt.cm.gray)
ax2.set_title('Canny edges')
ax2.set_axis_off()
ax2.set_adjustable('box-forced')

ax3.imshow(edges * 0, cmap=plt.cm.gray)

for line in lines:
    p0, p1 = line
    ax3.plot((p0[0], p1[0]), (p0[1], p1[1]))

ax3.set_title('aProbabilistic Hough')
ax3.set_axis_off()
ax3.set_adjustable('box-forced')
plt.show()

plt.savefig('A3.eps', format='eps', dpi=1000)

### get area between busbars
dims = extractedImg.shape
A = []
# orders the lines found by column value
lines = sorted(lines)

#removes lines that are not nearly vertical (cracks, upper and lower edges ignored)
for k in range(len(lines)):
    if (abs(lines[k][0][0] - lines[k][1][0])) > 5:
        lines.remove(lines[k])
        
# Gets areas between busbars
for i in range(len(lines)):
    if i == 0:
        A.append(extractedImg[:,0:lines[i][0][0]])
    if (i > 0) & (i < (len(lines)-1)):
        if i%2 != 0:
            A.append(extractedImg[:,lines[i][0][0]:lines[i+1][0][0]])
    if i == (len(lines)-1):
        A.append(extractedImg[:,lines[i][0][0]:dims[1]])
        
fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(A[3], cmap=plt.cm.gray, interpolation='none')
   
# A bit of analysis. Simple dark area percentage in regions. 
DarkPercent = []
for j in range(len(A)):
    shape = A[j].shape
    area = shape[0]*shape[1]
    darkSpots = np.where(A[j] == 0)
    sizeDarkSpots = len(darkSpots[0])
    DarkPercent.append(sizeDarkSpots/area)
print (DarkPercent)

## 20-180 on y only. Crude way to cut out dark corners.
DarkPercent = []
for j in range(len(A)):
    shape = A[j].shape
    area = 160*shape[1]
    darkSpots = np.where(A[j][20:180,:] == 0)
    sizeDarkSpots = len(darkSpots[0])
    DarkPercent.append(sizeDarkSpots/area)
print (DarkPercent)

fig, ax = plt.subplots(figsize=(4, 3))
ax.imshow(A[0], cmap=plt.cm.gray, interpolation='none')   
    
    
    

    
    
#for i in range(len(lines)):
#    p0 = lines[i][0][0]
#    if i == 0:
#        if p0 < .05*extractedImg.shape[1]:
#            A.append(extractedImg[:,p0:lines[i+1][0][0]])
#        else:
#            A.append(extractedImg[:,0:lines[i+1][0][0]])
#    if (i > 0) & (i < len(lines)):
#        
#    
#    if i == len(lines):
#        if p0 > .9*extractedImg.shape[1]:
#            A
        
   

##### The loop extract of all 4 cells
#for i in range(1, (num_features+1)):
#    print i
#    locationY, locationX = np.where(labeled_array == i)
#    Xmin = np.min(locationX)
#    Xmax = np.max(locationX)
#    Xdiff = Xmax - Xmin
#    Ymin = np.min(locationY)
#    Ymax = np.max(locationY)
#    Ydiff = Ymax - Ymin
#    canvas = np.zeros([Ydiff+1,Xdiff+1])
#    dims = locationX.shape
#    if (dims[0] >= 400):
#        for j in range(dims[0]):
#            canvas[(locationY[j]-Ymin),(locationX[j]-Xmin)] = grayModuleImgTest[locationY[j],locationX[j]]
#        fig, ax = plt.subplots(figsize=(4, 3))
#        ax.imshow(canvas, cmap=plt.cm.gray, interpolation='none')
#    
        
        #reducedCanvas = skimage.transform.resize(canvas, [120,120])
        #string = os.path.join(savePath, filePath[-24:-4] + '-cell' + str(i) + '.jpg')
        #scipy.misc.imsave(string, reducedCanvas)
        
 